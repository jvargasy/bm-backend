const cors = require('cors');
require('dotenv').config();
const express = require('express');
const body_parser = require('body-parser');
const app = express();
const port = process.env.PORT || 3003;

app.use(body_parser.json());

var enableCors = function(req,res,next){
    res.set("Access-Control-Allow-Origin","*");
    res.set("Access-Control-Allow-Methods","POST,GET,OPTIONS,DELETE,PUT");
    res.set("Access-Control-Allow-Headers","*");
    next();
}

app.use(enableCors);

require("./app/routes/cuenta.routes.js")(app);
require("./app/routes/movimiento.routes.js")(app);
require("./app/routes/usuario.routes.js")(app);
require("./app/routes/apiexterna.routes.js")(app);
require("./app/routes/tarjeta.routes.js")(app);

 app.listen(port, () => {
  console.log(`Node JS escuchando en el puerto ${port}`);
});
