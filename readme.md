#API REST Proyecto final TechU practioner 2020-2
==============================================

Todo el código esta alojado en GitHub en el siguiente repositorio: [API REST adi] (https://bitbucket.org/jvargasy/bm-backend/src/master/)

#Introducción
=============
Esta API está desarrollado para el proyecto Practitioner BBVA Perú segunda edición 2020. Aplicación web que realiza transferencias, envío correo electrónico, lista tarjetas, lista movimientos, muestra cuentas, busca cuentas, toda la funcionalidad a través de un logueo y si el usuario es nuevo realiza la afiliación y puede recuperar contraseñas.
Usa una API externa para mostrar el tipo de cambio ($) del día.

#Casos de Uso
=============

    GET  
   ![alt_text](readme/caso1.png)
   ![alt_text](readme/caso2.png)
   ![alt_text](readme/caso3.png)
   ![alt_text](readme/caso4.png)
   ![alt_text](readme/caso5.png)
   ![alt_text](readme/caso6.png)
   ![alt_text](readme/caso7.png)
   ![alt_text](readme/caso8.png)
   ![alt_text](readme/caso9.png)
   ![alt_text](readme/caso10.png)

    POST
   ![alt_text](readme/caso11.png)
   ![alt_text](readme/caso12.png)
   ![alt_text](readme/caso13.png)
   ![alt_text](readme/caso14.png)
   ![alt_text](readme/caso15.png)
   ![alt_text](readme/caso16.png)

#Lista de comandos para puesta en marcha
=======================================
Para poner en marcha la aplicación tendremos que realizar los siguientes comandos:

    1. Clonar el proyecto bm-backend :
        git clone https://JoanaKaren@bitbucket.org/jvargasy/bm-backend.git

    2. Se crea la carpeta una bm-backend luego:
        npm install //con esto instalaremos los modulos necesarios para node.js en el backend.

    3. Crear el archivo ( .env ) en el backend con los siguientes datos:
        API_KEY_MLAB=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF
        API_KEY_EXTERNA=5007|44NtAE8vF~PXxUGfPWaVXVz3CR6B5L5_
        KEY_EMAIL=TechU2017

    4.  Instalamos el nodemailer, para el envío de correo electrónico.
        npm install nodemailer

    5. Hay que levantar el servidor en una terminal:
        En la carpeta bm-backend : npm run prod

    Para pruebas de los APIs se uso Postman.
