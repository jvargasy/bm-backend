#Imagen docker base inicial
FROM node:latest

#crear directorio de trabajo del contenedor docker
WORKDIR /docker-dir-apitechu

#copiar archivos del proyecto en el directorio trabajo docker
ADD . /docker-dir-apitechu

#Instalar dependencias produccion del proyecto
#RUN nmp install --only=production

#Puerto donde exponemos contenedro (mismo que usamos en nuestra API)
EXPOSE 3003

#comando para lanzar la app
CMD ["npm","run","prod"]
