module.exports = app => {
    const usuarios = require("../controllers/usuario.controllers.js");
    const URL_BASE = '/techu/v1/';

    /* uri/techu/usuarios/cuentas/:id_cuenta/movimientos*/
    app.get(URL_BASE + "usuarios/:id_usuario/cuentas", usuarios.mostrarCuentasPorUsuario);
    /* uri/techu/usuarios/:id_usuario*/
    app.get(URL_BASE + "usuarios/:id_usuario", usuarios.buscarPorId);
    /* uri/techu/usuarios/login*/
    app.post(URL_BASE + "usuarios/login", usuarios.logueo);
    /* uri/techu/usuarios/logout*/
    app.post(URL_BASE + "usuarios/logout", usuarios.logout);
    /* uri/techu/usuarios/altabm/:tipo:doc*/
    app.post(URL_BASE + "usuarios/altabm/:tipo:doc", usuarios.altabm);
    /* uri/techu/usuarios/password/:tipo/:doc/:username*/
    app.get(URL_BASE + "usuarios/password/:tipo/:doc/:username", usuarios.recuperarpass);

    app.post(URL_BASE + "utilitario/enviarEmail", usuarios.envioConstancia);
}