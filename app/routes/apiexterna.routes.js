module.exports = app => {
    const apiexterna = require("../controllers/apiexterna.controllers.js");
    const URL_BASE = '/techu/v1/';

    /* uri/techu/usuarios/tipocambio*/
    app.get(URL_BASE + "apiExterna/tipocambio", apiexterna.tipocambio);
}
