module.exports = app => {
    const tarjetas = require("../controllers/tarjeta.controllers.js");
    const URL_BASE = '/techu/v1/';

    app.get(URL_BASE + "usuarios/cuentas/tarjetas", tarjetas.listarTarjetas);

}