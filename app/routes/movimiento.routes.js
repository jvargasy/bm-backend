module.exports = app => {
    const movimientos = require("../controllers/movimiento.controllers.js");
    const URL_BASE = '/techu/v1/';

    /* uri/techu/usuarios/:id_usuario/cuentas/:id_cuenta/movimiento*/
    app.post(URL_BASE + "usuarios/:id_usuario/cuentas/:id_cuenta/movimiento",movimientos.insertarMovimiento);
    /* uri/techu/usuarios/cuentas/:id_cuenta/movimientos*/
    app.get(URL_BASE + "usuarios/cuentas/:id_cuenta/movimientos", movimientos.mostrarMovimientos);
    /* uri/techu/usuarios/cuentas/movimientos/tarjetas*/
    app.get(URL_BASE + "usuarios/:id_usuario/cuentas/filtrado", movimientos.mostrarSoloCuentas);

    app.get(URL_BASE + "usuarios/cuentas/movimientos", movimientos.mostrarMovimientosFiltro);

}
