module.exports = app => {
    const cuentas = require("../controllers/cuenta.controllers.js");
    const URL_BASE = '/techu/v1/';

    /* uri/techu/usuarios/cuentas/:id_cuenta/movimientos*/
    app.get(URL_BASE + "usuarios/cuentas/:id_cuenta/tarjetas", cuentas.mostrarTarjetas);

    /* uri/techu/cuentas/filtrado?nroCuenta=xxxxxx*/
    app.get(URL_BASE + "usuarios/cuentas/filtrado", cuentas.buscarCuenta);

    app.post(URL_BASE + "usuarios/altaMovimiento", cuentas.altaMovimiento);

    app.get(URL_BASE + "usuarios/cuenta/:id_cuenta", cuentas.buscarCuentaByID);
}