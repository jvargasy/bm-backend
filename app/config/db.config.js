module.exports = {
    URL_MLAB: "https://api.mlab.com/api/1/databases/techu14db/collections/",
    URL_TCMONEDAS: "https://api.cambio.today/v1/quotes/",
    APIKEY: "apiKey=" + process.env.API_KEY_MLAB,
    APIKEYEXTERNA: "key=" + process.env.API_KEY_EXTERNA,
    EMAILKEY: process.env.KEY_EMAIL
};
