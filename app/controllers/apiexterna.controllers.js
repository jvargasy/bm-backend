const dbConfig = require("../config/db.config.js");
const request_json = require('request-json');
const util = require("../util/util.js");
const clienteMlab = request_json.createClient(dbConfig.URL_TCMONEDAS);

/* uri/techu/usuarios/tipocambio*/
exports.tipocambio = (request, response) => {
  clienteMlab.get(dbConfig.URL_TCMONEDAS + 'USD/PEN/json?' + dbConfig.APIKEYEXTERNA,
    function(error1,respuestaMlab,body){
      var respuesta = body;
      if(error1){
        response.status(500).send({"msgError" : "Error  en la petición a mLab."});
      }else{
        if(respuesta!=undefined){
            response.status(200).send(respuesta);
        }else{
         response.status(404).send({"msg":"Datos no encontrados"});
        }
      }
  });
}
