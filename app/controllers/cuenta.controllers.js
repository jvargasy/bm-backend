const dbConfig = require("../config/db.config.js");
const request_json = require('request-json');
const util = require("../util/util.js");
const clienteMlab = request_json.createClient(dbConfig.URL_MLAB);

exports.mostrarTarjetas = (req, res) => {
    let query = 'q={"id_cuenta":'+req.params.id_cuenta+'}&';
    util.mostrarTodosRecursos(req, res, query,"tarjetas",dbConfig.APIKEY);
}

exports.buscarCuenta = (req, res) => {
    let query = 'q={"numero_cuenta":'+req.query.nroCuenta+'}&';
    util.mostrarTodosRecursos(req, res, query,"cuentas",dbConfig.APIKEY);
}

exports.buscarCuentaByID = (req, res) => {
    let query = 'q={"id_cuenta":'+req.params.id_cuenta+'}&';
    util.mostrarTodosRecursos(req, res, query,"cuentas",dbConfig.APIKEY);
}

exports.altaMovimiento = (req, res) => {
    var idCtaOrigen = req.body.idCtaOrigen;
    var idCtaDestino = req.body.idCtaDestino;
    var importe = req.body.importe;
    var concepto = req.body.concepto;
    var tipoCambio = req.body.tipocambio;
    var cambioMoneda = req.body.cambioMoneda;
    var fechaOper = req.body.fecha;
    var nroOperacion = req.body.nroOperacion;

    var queryIdCuenta = 'q={"numero_cuenta": "' + idCtaOrigen+ '"}&';


/*    var session = {"login.estado":true};
    var login = '{"$set":' + JSON.stringify(session) + '}';*/
    console.log('cuentas?'+ queryIdCuenta + dbConfig.APIKEY);
    clienteMlab.get('cuentas?'+ queryIdCuenta + dbConfig.APIKEY,
        function(errorCtaOrigen,respuestaMlab,body){
            if(errorCtaOrigen){
                res.status(500).send({"msgError" : "Error  en la petición a mLab cta Origen."});
            }else{
                console.log("Inicio al obtener datos de operacion Cuenta Origen");
                var respuesta =  body[0];
                if (respuesta!=undefined){
                    if(respuesta.saldo>=importe){
                        var nuevoSaldo= parseFloat(respuesta.saldo) - parseFloat(importe);
                        var transferir = {"saldo":nuevoSaldo.toFixed(2)}
                        var operacion = '{"$set":' + JSON.stringify(transferir) + '}';
                        var divisaCtaOrigen = respuesta.id_divisa;
                        //var factorCambio = 1;
                       /* var session = {"login.estado":true};
                        var login = '{"$set":' + JSON.stringify(session) + '}';*/

                        clienteMlab.put('cuentas?q={"id_cuenta": ' + respuesta.id_cuenta+ '}&' + dbConfig.APIKEY,JSON.parse(operacion),
                            function(error2,respuestaMlab,body){
                                if(error2){
                                    res.status(500).send({"msgError" : "Error  en la petición a mLab ctaOrigen."});
                                }else{
                                    console.log("concepto: " + concepto);
                                    //res.status(200).send(respuesta);
                                    var queryIdCuentaDestino = 'q={"numero_cuenta": "' + idCtaDestino+ '"}&';
                                    clienteMlab.get('cuentas?'+ queryIdCuentaDestino + dbConfig.APIKEY,
                                        function(errorCtaDestino,respuestaMlab,body){
                                            if(errorCtaDestino){
                                                res.status(500).send({"msgError" : "Error  en la petición a mLab cta Destino."});
                                            }else{
                                                console.log("Inicio al obtener datos de OPE CTA Destino");
                                                var rtaCtaDestino =  body[0];
                                                if (rtaCtaDestino!=undefined){
                                                    console.log("saldo ini des: "+rtaCtaDestino.saldo);
                                                    //Actualizar importe------------
                                                    var divisaCtaDestino = rtaCtaDestino.id_divisa;

                                                    divisaDes = util.transformarDivisa(divisaCtaDestino);
                                                    divisaOrig = util.transformarDivisa(divisaCtaOrigen);
                                                        clienteMlab.get(dbConfig.URL_TCMONEDAS + divisaOrig+'/'+divisaDes+'/json?' + dbConfig.APIKEYEXTERNA,
                                                            function(errorCambio,respuestaMlab,body){
                                                                var respuestaCambio = body;
                                                                if(errorCambio){
                                                                    res.status(500).send({"msgError" : "Error  al obtener el factor divisa"});
                                                                }else{
                                                                    if(respuestaCambio!==undefined){
                                                                        var factorCambioActual = parseFloat(respuestaCambio.result.amount);
                                                                        var nuevoSaldoDestino= parseFloat(rtaCtaDestino.saldo) + (parseFloat(importe)*factorCambioActual);
                                                                        var transferirDestino = {"saldo":nuevoSaldoDestino.toFixed(2)};
                                                                        var operacionDestino = '{"$set":' + JSON.stringify(transferirDestino) + '}';
                                                                        //response.status(200).send(respuesta);
                                                                        clienteMlab.put('cuentas?q={"id_cuenta": ' + rtaCtaDestino.id_cuenta+ '}&' + dbConfig.APIKEY,JSON.parse(operacionDestino),
                                                                            function (errorPutCtaDestino,respuestaMlab,body){
                                                                                if(errorPutCtaDestino){
                                                                                    res.status(500).send({"msgError" : "Error  en la petición a mLab ctaDestino."});
                                                                                }else{
                                                                                    var rta = {"ctaOrigen":respuesta,
                                                                                        "ctaDestino":rtaCtaDestino};
                                                                                    //res.status(200).send(rta);
                                                                                    var movimientoTmp = {
                                                                                        "id_movimiento": util.generarId(),
                                                                                        "id_cuenta": respuesta.id_cuenta,
                                                                                        "concepto": concepto,
                                                                                        "fecha_operacion": fechaOper,
                                                                                        "importe": parseFloat(importe)*(-1),
                                                                                        "nro_operacion":nroOperacion
                                                                                    }
                                                                                    var cambiomonedaDes = factorCambioActual;
                                                                                    //console.log('movimientos?&' + dbConfig.APIKEY,JSON.stringify(movimientoTmp));
                                                                                    clienteMlab.post(dbConfig.URL_MLAB + 'movimientos?' + dbConfig.APIKEY,movimientoTmp,
                                                                                        function (errorMovimientoCtaOrigen,respuestaMlab,body){
                                                                                            if(errorMovimientoCtaOrigen){
                                                                                                res.status(500).send({"msgError" : "Error  en la petición a mLab movimiento Origen."});
                                                                                            } else{
                                                                                                console.log("Insertar movimiento cta origen");
                                                                                                //res.status(200).send(movimientoTmp);
                                                                                                console.log(movimientoTmp);
                                                                                                movimientoTmp.id_cuenta = rtaCtaDestino.id_cuenta;
                                                                                                movimientoTmp.id_movimiento = util.generarId();
                                                                                                movimientoTmp.importe = (parseFloat(importe)*cambiomonedaDes).toFixed(2);
                                                                                                clienteMlab.post(dbConfig.URL_MLAB + 'movimientos?' + dbConfig.APIKEY,movimientoTmp,
                                                                                                    function (errorMovimientoCtaDestino,respuestaMlab,body){
                                                                                                        if(errorMovimientoCtaDestino){
                                                                                                            res.status(500).send({"msgError" : "Error  en la petición a mLab movimiento Destino."});
                                                                                                        }else{
                                                                                                            console.log(movimientoTmp);
                                                                                                            res.status(204).send();
                                                                                                        }
                                                                                                    });
                                                                                            }
                                                                                        });
                                                                                }
                                                                            });
                                                                    }else{
                                                                        res.status(404).send({"msg":"Datos no encontrados para divisa"});
                                                                    }
                                                                }
                                                            });

                                                }else{
                                                    res.status(200).send({"msg":"Cuenta Destino incorrecta"});
                                                }

                                            }
                                        });
                                }


                            });
                    }
                    else{
                        res.status(200).send({"msg":"Saldo Insuficiente"});
                    }
                }else{
                    res.status(200).send({"msg":"Cuenta Origen incorrecta"});
                }
            }
        });
}