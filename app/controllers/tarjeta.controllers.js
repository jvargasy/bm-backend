const dbConfig = require("../config/db.config.js");
const request_json = require('request-json');
const util = require("../util/util.js");

exports.listarTarjetas = (req, res) => {
    let query = 'q={"id_cuenta":{$in:['+req.query.filtro+']}}&';
    util.mostrarTodosRecursos(req, res, query,"tarjetas",dbConfig.APIKEY);
}