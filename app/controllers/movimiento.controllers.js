const dbConfig = require("../config/db.config.js");
const request_json = require('request-json');
const util = require("../util/util.js");

/* uri/techu/usuarios/:id_usuario/cuentas/:id_cuenta/movimiento*/
exports.insertarMovimiento = (req, res) => {
    console.log("insertar movimientos");
    var nuevoMovimiento = {
        "id_movimiento": util.generarId(),
        "id_cuenta": parseInt(req.params.id_cuenta),
        "id_tarjeta": null, /*Obtener tarjeta desde uri o function intena que obtenga el id de la tarjeta en base a la cuenta*/
        "fecha_operacion": req.body.fecha_operacion,
        "importe": req.body.importe
    };
    let clienteMlab = request_json.createClient(dbConfig.URL_MLAB);
    console.log(dbConfig.URL_MLAB +'movimientos?'+ dbConfig.APIKEY);
    clienteMlab.post(dbConfig.URL_MLAB +'movimientos?'+ dbConfig.APIKEY, nuevoMovimiento,
        function (error) {
            if(error){
                response = {"msgError" : "Error  en la petición a mLab."};
                res.status(500);
                console.log("Error");
            } else {
                response = {"msg" : "Creacion exitosa"};
                res.status(201);
                console.log("creacion ok");
            }
            res.send(response);
        });
}

exports.mostrarMovimientos = (req, res) => {
    let query = 'q={"id_cuenta":'+req.params.id_cuenta+'}&';
    let sort  = 's={"id_movimiento":-1}&'
    util.mostrarTodosRecursos(req, res, query+sort,"movimientos",dbConfig.APIKEY);
}

exports.mostrarSoloCuentas = (req, res) => {
    let query = 'q={"id_usuario":'+req.params.id_usuario+'}&';
    let filter = 'f={"_id":0,"numero_cuenta":0,"id_divisa":0,"tipo_cuenta":0,"saldo":0,"id_usuario":0}&';
    util.mostrarTodosRecursos(req, res,query + filter,"cuentas",dbConfig.APIKEY);
}

exports.mostrarMovimientosFiltro = (req,res) => {
    let query = 'q={"id_cuenta":{$in:['+req.query.filtro+']}}&';
    util.mostrarTodosRecursos(req, res,query,"movimientos",dbConfig.APIKEY);
}

