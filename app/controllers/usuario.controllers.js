const dbConfig = require("../config/db.config.js");
const request_json = require('request-json');
const util = require("../util/util.js");
const nodemailer = require('nodemailer');
const clienteMlab = request_json.createClient(dbConfig.URL_MLAB);

exports.mostrarCuentasPorUsuario = (req, res) => {
    let query = 'q={"id_usuario":'+req.params.id_usuario+'}&';
    util.mostrarTodosRecursos(req, res, query,"cuentas",dbConfig.APIKEY);
}

/* uri/techu/usuarios/login*/
exports.logueo = (request, res) => {
    var username = request.body.username;
    var password = request.body.password;

    var queryStringUserName = 'q={"login.username": "' + username + '"}&';
    var queryStringPass = 'q={"login.password":"' + password + '"}&';
    clienteMlab.get('usuarios?'+ queryStringUserName + queryStringPass + dbConfig.APIKEY,
        function(error1,respuestaMlab,body){
            if(error1){
                res.status(500).send({"msgError" : "Error  en la petición a mLab."});
            }else{
                var respuesta =  body[0];
                if (respuesta!=undefined){
                    if(respuesta.login.password==password){
                        var session = {"login.estado":true};
                        var login = '{"$set":' + JSON.stringify(session) + '}';
                        clienteMlab.put('usuarios?q={"id_usuario": ' + respuesta.id_usuario+ '}&' + dbConfig.APIKEY,JSON.parse(login),
                            function(error2,respuestaMlab,body){
                                if(error2){
                                    res.status(500).send({"msgError" : "Error  en la petición a mLab."});
                                }else{
                                    res.status(200).send(respuesta);
                                }
                            });
                    }
                    else{
                        res.status(200).send({"msg":"contraseña incorrecta"});
                    }
                }else{
                    res.status(200).send({"msg":"usuario incorrecto"});
                }
            }
        });
}

/* uri/techu/usuarios/logout*/
exports.logout = (request, response) => {
    var username = request.body.username;
    var queryStringUserName = 'q={"login.username": "' + username + '"}&';

    clienteMlab.get('usuarios?' + queryStringUserName + dbConfig.APIKEY,
        function(error1,respuestaMlab,body){
            if(error1){
                response.status(500).send({"msgError" : "Error  en la petición a mLab."});
            }else{
                var respuesta = body[0];
                if(respuesta!=undefined){
                    var session = {"login.estado":false};
                    var logout = '{"$set":' + JSON.stringify(session) + '}';

                    clienteMlab.put('usuarios?q={"id_usuario": ' + respuesta.id_usuario + '}&' + dbConfig.APIKEY,JSON.parse(logout),
                        function(error2,resMlab,bod){
                            if(error2){
                                response.status(500).send({"msgError" : "Error  en la petición a mLab."});
                            }else{
                                response.status(200).send({"msg":"Gracias por usar la Banca BBVA"});
                            }
                        });

                }else{
                    response.status(200).send({"msg":"Usuario no encontrado"});
                }
            }

        });
}

/* uri/techu/usuarios/altabm/:tipo:doc*/
exports.altabm = (request, response) => {
    var tipo = request.params.tipo;
    var doc = request.params.doc;
    var queryStringDocumento= 'q={"id_tipo_doc":' + tipo + ',"numero_doc": ' + doc +'}&';
    clienteMlab.get('usuarios?' + queryStringDocumento + dbConfig.APIKEY,
        function(error1,respuestaMlab,body){
            var respuesta = body[0];
            if(error1){
                response.status(500).send({"msgError" : "Error  en la petición a mLab."});
            }else{
                if(respuesta!=undefined){
                    var altaUusuarioBM = {
                        "email": request.body.email,
                        "login.username": request.body.username,
                        "login.password":request.body.password,
                        "login.estado": false
                    };
                    var alta = '{"$set":' + JSON.stringify(altaUusuarioBM) + '}';

                    if(respuesta.login.username == ""){
                      clienteMlab.put('usuarios?q={"id_usuario": ' + respuesta.id_usuario + '}&' + dbConfig.APIKEY,JSON.parse(alta),
                          function(error2,resMlab,bod){
                              if(error2){
                                  response.status(500).send({"msgError" : "Error  en la petición a mLab."});
                              }else {
                                  response.status(201).send({"msg":"Bienvenido a la Banca BBVA"});
                              }
                          } );
                    }else{
                      response.status(200).send({"msg":"Cliente ya dado de alta en la Banca BBVA"});
                    }
                }else{
                    response.status(200).send({"msg":"No es cliente BBVA"});
                }
            }
        });
}

/* uri/techu/usuarios/password/:tipo/:doc/:username*/
exports.recuperarpass = (request, response) => {
    var tipo = request.params.tipo;
    var doc = request.params.doc;
    var username = request.params.username;

    var queryStringDocumento= 'q={"id_tipo_doc":' + tipo + ',"numero_doc": ' + doc +',"login.username":"' + username + '"}&';
    clienteMlab.get('usuarios?' + queryStringDocumento + dbConfig.APIKEY,
        function(error1,respuestaMlab,body){
            var respuesta = body[0];
            if(error1){
                response.status(500).send({"msgError" : "Error  en la petición a mLab."});
            }else{
                if(respuesta!=undefined){
                    if(respuesta.login.username==""){
                        response.status(200).send({"msg":"Debe darse de alta en la Banca BBVA"});
                    }else{
                        sendEmail(request, response,respuesta);
                    }
                }else{
                    response.status(200).send({"msg":"No es cliente BBVA o el usuario no es correcto"});
                }
            }
        });
}


exports.envioConstancia = (request, response) => {
    sendEmailConstancia(request, response);
}

function sendEmail(req, res, recurso){
    var correoSend = recurso.email;
    var passwordSend = recurso.login.password;
    console.log(correoSend);
    console.log(passwordSend);
    // Definimos el transporter
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'proyectotechU2020@gmail.com',
            pass: dbConfig.EMAILKEY
        }
    });
    // Definimos el email
    var mailOptions = {
        from: 'Banca TechU BBVA',
        to: correoSend,
        subject: 'Recuperar Contraseña',
        text: 'Estimado Cliente su contraseña es la siguiente: ' + passwordSend
    };
    // Enviamos el email
    transporter.sendMail(mailOptions, function(error, info){
        if (error){
            res.send(500, error.message);
        } else {
            res.status(200).send({"msg":"Estimado Cliente su password ha sido enviado al correo registrado"});
        }
    });
}


function sendEmailConstancia(req, res){
    let correoSend = req.body.email;
    let nombreTitular = req.body.nombre;
    let importe = req.body.importe;
    let numOperacion = req.body.numOperacion;
    let hora = req.body.hora;
    let beneficiario = req.body.beneficiario;
    let concepto = req.body.concepto;


    // Definimos el transporter
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'proyectotechU2020@gmail.com',
            pass: dbConfig.EMAILKEY
        }
    });
    // Definimos el email
    var mailOptions = {
        from: 'Banca TechU BBVA',
        subject: 'BBVA - Constancia TRANSFERENCIA',
        to: correoSend,
        html:'<h2>Hola,'+nombreTitular+'</h2><br> has realizado con éxito la operación:' +
            '<br><h4>Transferencia</h4><br>Importe transferido<h3>'+importe+'</h3>' +
            '<br>Número de operación:<h3>'+numOperacion +' </h3><br>Hora:<h3>'+hora+'</h3><br>Cuenta Beneficiario<h3>'+beneficiario+'</h3><br>Concepto<h3>'+concepto+'</h3>'
    };

    // Enviamos el email
    transporter.sendMail(mailOptions, function(error, info){
        if (error){
            res.send(500, error.message);
        } else {
            res.status(204).send("");
        }
    });
}

exports.mostrarUltimosMovimientos = (req, res)=>{
//    mostrarCuentasPorUsuario = (req, res) => {
        let query = 'q={"id_usuario":'+req.params.id_usuario+'}&';
        util.mostrarTodosRecursos(req, res, query,"movimientos",dbConfig.APIKEY);

}
exports.buscarPorId = (req, res) => {
    let query = 'q={"id_usuario":'+req.params.id_usuario+'}&';
    util.mostrarTodosRecursos(req, res, query,"usuarios",dbConfig.APIKEY);
}
