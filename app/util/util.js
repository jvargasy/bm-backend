const request_json = require("request-json");
const dbConfig = require("../config/db.config");
exports.generarId = ()=>{
    return new Date().valueOf();
};

exports.mostrarTodosRecursos = (req, res, query, recurso, apikey) =>{
    let http_client = request_json.createClient(dbConfig.URL_MLAB);
    let field_param = 'f={"_id":0}&';
    var mostrar = {
        "response": null,
        "status": null
    };
    /*Agregar Descripcion del movimiento*/
    console.log(recurso +"?"+ query+ field_param + apikey);
    http_client.get(recurso +"?"+ query+ field_param + apikey,
        function (err, respuestaMLab, body) {
            let response = {};
            if(err) {
                res.status(500);
            } else {
                if(body.length > 0) {
                    response = body;
                } else {
                    response ={"msg" : "Recurso no encontrado."};
                    res.status(404);
                }
            }
            res.send(response);
        });
};

exports.transformarDivisa = (idDivisa) =>{
    switch (idDivisa) {
        case 1:
            return 'PEN';
        case 2:
            return 'USD';
        case 3:
            return 'EUR';
    }
}




